import React, {Component} from 'react';
import RequestAccountForm from './ManageAccounts';

export default class App extends Component {
  render() {
    const maxWidth = {
      maxWidth: "1500px"
    };
    return (
      <div
        className="container-fluid"
        style={maxWidth}
      >
        <div className="jumbotron">
          <h1>PII Librarian Demo</h1>
        </div>
        <RequestAccountForm/>
      </div>
    );
  }
};