import React, {Component} from 'react';
import { Line } from 'rc-progress';
import AccountsApi from '../api/AccountsApi';
import autoBind from 'auto-bind';
import { ProgressBar } from 'react-bootstrap';

export default class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            accounts: [],
            searchInput: '',
            forgetInput: '',
            blockchainData: [],
            loading: true,
            loadingValue: 0,
            startingView: true
        };
        autoBind(this);
    }

    componentDidMount() {
        AccountsApi.unforgetAccounts().then(response => {
            console.log(response);
        }).catch(reason => {
            console.log(reason);
        });

        AccountsApi.getAccounts().then(response => {
            console.log(response);
            this.setState({
                accounts: response
            });
        }).catch(reason => {
            console.log(reason);
        });
    }

    getLatestBlockchainData() {
        return AccountsApi.getBlockchainData().then(response => {
            console.log(response);

            this.setState({ blockchainData: response });
        }).catch(reason => {
            console.log(reason);
        })
    }

    getAllAccounts() {
        this.startLoading();

        AccountsApi.getAccounts().then(response => {
            console.log(response);
            this.setState({
                accounts: response
            });
        }).catch(reason => {
            console.log(reason);
        });

        AccountsApi.recordToBlockchain().then(response => {
            this.setState({loadingValue: 100});

            setTimeout(() => {
                this.setState({loading: false});
            }, 1000);

            this.getLatestBlockchainData();
        }).catch(reason => {
            console.log(reason);
        });
    }

    getAccountById() {
        this.startLoading();

        AccountsApi.getAccount(this.state.searchInput).then(response => {
            this.setState({
                accounts: response
            });
        }).catch(reason => {
            console.log(reason);
        });

        AccountsApi.recordSingleRecordToBlockchain(this.state.searchInput).then(response => {
            this.setState({loadingValue: 100});

            setTimeout(() => {
                this.setState({loading: false});
            }, 1000);

            this.getLatestBlockchainData();
        }).catch(reason => {
            console.log(reason);
        });
    }

    forgetAccountById() {
        AccountsApi.forgetAccount(this.state.forgetInput).then(response => {
            console.log(response);
            this.getAllAccounts();
        }).catch(reason => {
            console.log(reason);
        });
    }

    startLoading() {
        this.setState({
            loading: true,
            blockchainData: [],
            loadingValue: 0,
            startingView: false
        });

        let currentLoad = 0;
        let interval = setInterval(() => {
            if(currentLoad > 90){
                clearInterval(interval);
            } else {
                currentLoad++;
                this.setState({loadingValue: currentLoad});
            }
        }, 500);
    }

    resetDemo() {
        this.setState({
            searchInput: '',
            forgetInput: '',
            blockchainData: [],
            loading: true,
            loadingValue: 0,
            startingView: true
        });

        AccountsApi.unforgetAccounts().then(response => {
            console.log(response);
        }).catch(reason => {
            console.log(reason);
        });

        AccountsApi.getAccounts().then(response => {
            console.log(response);
            this.setState({
                accounts: response
            });
        }).catch(reason => {
            console.log(reason);
        });
    }

    handleSearchInputChange(e) {
        this.setState({searchInput: e.target.value});
    }

    handleForgetInputChange(e) {
        this.setState({forgetInput: e.target.value});
    }

    handleSearchButtonPress(e) {
        e.preventDefault();
        this.getAccountById();
    }

    handleForgetButtonPress(e) {
        e.preventDefault();
        this.forgetAccountById();
    }

    handleResetDemoButtonPress(e) {
        e.preventDefault();
        this.resetDemo();
    }

    render() {
        const spacing5 = {
            marginRight: "5px"
        };

        const spacing30 = {
            marginRight: "30px"
        };

        const width150 = {
            width: "130px",
            marginTop: "5px"
        };

        const alignRight = {
            float: "right"
        };

        return (
            <div className="container-fluid">
                <form className="form-inline">
                    <label className="sr-only" htmlFor="searchAccountByID">Find Account By ID</label>
                    <input style={spacing5} type="text" className="form-control" id="searchAccountByID"
                           placeholder="Find Account By ID" onChange={this.handleSearchInputChange} />
                    <button style={spacing30} className="btn btn-primary mr-2" onClick={this.handleSearchButtonPress}>Search</button>

                    <label className="sr-only" htmlFor="forgetByAccountID">Forger Account By ID</label>
                    <input style={spacing5} type="text" className="form-control" id="forgetByAccountID"
                           placeholder="Forget Account By ID" onChange={this.handleForgetInputChange} />
                    <button className="btn btn-primary" onClick={this.handleForgetButtonPress}>Forget</button>
                    <button style={alignRight} className="btn btn-primary" onClick={this.handleResetDemoButtonPress}>Reset Demo</button>
                </form>
                <hr />
                <div className="row">
                    <h3 className="col-sm-2 col-form-label" style={width150}>Accounts</h3>
                    <button className="btn btn-primary" onClick={this.getAllAccounts}>Reload</button>
                </div>
                <div>
                    <table className="table">
                        <thead>
                        <tr>
                            <th>Account Number</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Phone Number</th>
                            <th>Balance</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.accounts && this.state.accounts.map(account => {
                            return (<tr>
                                <td scope="col">{account.accountNumber}</td>
                                <td scope="col">{account.name}</td>
                                <td scope="col">{account.address}</td>
                                <td scope="col">{account.phoneNumber}</td>
                                <td scope="col">{account.balance}</td>
                            </tr>);
                        })
                        }
                        </tbody>
                    </table>
                </div>
                <div hidden={this.state.startingView}>
                    <hr />
                    <div hidden={this.state.loading}>
                        <div>
                            <h3>Last Contract Data Stored In Blockchain</h3>
                        </div>
                        <div>
                            <table className="table">
                                <thead>
                                <tr>
                                    <th>Account Number</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Phone Number</th>
                                    <th>Balance</th>
                                </tr>
                                </thead>
                                <tbody>
                                {this.state.blockchainData && this.state.blockchainData.map(account => {
                                    return (<tr>
                                        <td scope="col">{account.accountNumber}</td>
                                        <td scope="col">{account.name}</td>
                                        <td scope="col">{account.address}</td>
                                        <td scope="col">{account.phoneNumber}</td>
                                        <td scope="col">{account.balance}</td>
                                    </tr>);
                                })
                                }
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div hidden={!this.state.loading}>
                        <h4>Storing The Requested Data In The Blockchain And Retrieving It For Display...</h4>
                        {/*<Line percent={this.state.loadingValue} strokeWidth="2" strokeColor="#428bca" strokeLinecap="square" trailWidth="2"/>*/}
                        <ProgressBar now={this.state.loadingValue} />
                    </div>
                </div>
            </div>
        );
    }
};