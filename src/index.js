import ReactDOM from 'react-dom';
import React from 'react';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App/>, document.getElementById('app'));
registerServiceWorker();