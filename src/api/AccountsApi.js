const librarianService = {
  uri: process.env.LIBRARIAN_SERVICE_URL || "https://librarian-service2.herokuapp.com"
};

export default class AccountsApi {
  static getAccounts() {
    return fetch(`${librarianService.uri}/accounts`, {}).then(res => {
      if (res.status === 200) {
        return res.json();
      }
      throw Error("Invalid response: " + res.status);
    });
  }

  static getBlockchainData() {
    return fetch(`${librarianService.uri}/blockchainData`, {})
      .then(res => {
        if (res.status === 200) {
          return res.json();
        }
        throw Error("Invalid response: " + res.status);
      })
      .catch(error => {
        console.error(error);
      });
  }

  static recordToBlockchain() {
    return fetch(`${librarianService.uri}/recordToBlockchain`, {'mode': 'no-cors'})
      .then(res => res.json())
      .catch(error => error);
  }

  static recordSingleRecordToBlockchain(id) {
      return fetch(`${librarianService.uri}/recordSingleRecordToBlockchain/${id}`, {'mode': 'no-cors'})
          .then(res => res.json())
          .catch(error => error);
  }

  static getAccount(id) {
    return fetch(`${librarianService.uri}/account/${id}`, {}).then(
      res => {
        if (res.status === 200) {
          return res.json();
        }
        throw Error("Invalid response: " + res.status);
      }
    );
  }

  static forgetAccount(id) {
    return fetch(librarianService.uri + `/forget/${id}`, {}).then(res => {
      if (res.status === 200) {
        return res;
      }
      throw Error("Invalid response: " + res.status);
    });
  }

  static unforgetAccounts() {
      return fetch(librarianService.uri + `/unforget`, {}).then(res => {
          if (res.status === 200) {
              return res;
          }
          throw Error("Invalid response: " + res.status);
      });
  }
}
